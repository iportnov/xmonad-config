import qualified Data.Map as M
import System.Environment
import System.FilePath
import Control.Concurrent (newChan)

import XMonad
import XMonad.Util.EZConfig (additionalKeysP)

-- import XMonad.Config.Gnome
import XMonad.Config.Desktop

import XMonad.Actions.GroupNavigation (historyHook)

-- Import hooks to support EWMH and other compatibility hooks
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.SetWMName    (setWMName)
import XMonad.Hooks.Minimize     (minimizeEventHook)
import XMonad.Hooks.Place
import XMonad.Util.Replace
import XMonad.Util.SpawnOnce
import XMonad.AppGroups     (apps2keys)
import XMonad.Pidgin
import XMonad.Store

-- My local modules (from ~/.xmonad/lib/)
import KeyBindings   (myMouseBindings, myKeys, addKeys)
import Layouts       (myLayout)
import MyManageHooks
import Themes
import CommonFunctions (unmapEventHook)
import GroupsSetup   (appsConfig)
import Remote
import Notify
import WacomConfig
import Info

-- import XMonad.TimeTracker
import System.Wacom.Types
import System.Wacom.Config
import XMonad.Wacom.Daemon
import XMonad.Wacom

------------------------------------------------------------------------
-- General settings
--
baseConfig = desktopConfig
baseManageHook = manageHook baseConfig
baseLogHook = logHook baseConfig

autostart :: X ()
autostart = do
  home <- io $ getEnv "HOME"
  let script = home </> ".xmonad" </> "autostart"
  spawn script

main =  do
  replace
  chan <- newChan
  xmonad $ ewmh $ baseConfig {
        terminal           = myTerminal,
        focusFollowsMouse  = False,
        borderWidth        = myBorderWidth,
        modMask            = mod4Mask,
        workspaces         = myWorkspaces,
        normalBorderColor  = inactiveDecoColor,
        focusedBorderColor = myFocusedBorderColor,
 
      -- key bindings
        keys               = myKeys,
        mouseBindings      = myMouseBindings,
 
      -- hooks, layouts
        layoutHook         = myLayout,
        handleEventHook    = unmapEventHook {- minimizeEventHook -} <+>  docksEventHook ,
--         handleXinputHook   = xinputHandler,
        manageHook         = (placeHook $ inBounds $ underMouse (0,0))
                             <+> baseManageHook
                             <+> useStoredProps
                             <+> pidginMoveByGroup pidginConfig
                             <+> myManageHook,
        startupHook        = do 
                                autostart
                                pidginConnect
                                -- trackerInit =<< (io defaultTrackerLog)
                                initWacom wconfig $ Callbacks notifyOnPlug notifyOnUnplug
                                io $ putStrLn "Wacom inited"
                                dbusExporter chan
                                readStoredProps ,
        logHook            = do
                                baseLogHook
                                exportInfo chan
--                                 updatePointer (TowardsCentre 0.5 0.5)
                                switchTabletProfile
                                historyHook
                                setWMName "LG3D"
--         focusNewWindow     = myFocusHook
    } `additionalKeysP` (addKeys ++ apps2keys appsConfig)

