
module Commands where

import Data.Maybe
import System.Environment (getEnv)
import System.FilePath
import System.FilePath.Glob

import XMonad
import XMonad.Actions.GridSelect

import Themes

textEditors = do
  let editors = ["nvim-qt", "kate", "gedit"]
  selected <- gridselect myGSConfig $ zip editors editors
  case selected of
    Nothing     -> return ()
    -- Just "gvim" -> vimsessions
    Just editor -> spawn editor

recent types = "recently-used.py " ++ unwords (map mime types)
  where
    mime t = fromMaybe "" $ lookup t pairs
    pairs = [("pdf", "application/pdf application/epub+zip"),
             ("djvu", "application/djvu"),
             ("doc", "application/msword"),
             ("png", "image/png")]

vimsession :: String -> X ()
vimsession name = do
  home <- io $ getEnv "HOME"
  let path = home </> ".vim/sessions" </> (name ++ ".vimsession")
  spawn ("gvim -S " ++ path)

vimsessions :: X ()
vimsessions = do
  home <- io $ getEnv "HOME"
  paths <- io $ concat `fmap` globDir [compile "*.vimsession"] (home </> ".vim/sessions")
  let sessions = map (dropExtension . takeFileName) paths
  selected <- gridselect myGSConfig $ zip sessions sessions
  whenJust selected vimsession
