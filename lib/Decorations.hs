{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses #-}

module Decorations where 

import XMonad
import XMonad.StackSet (Stack (..))
import XMonad.Layout.Decoration

data FloatingOnly a = FloatingOnly
  deriving (Show, Read)

instance Eq a => DecorationStyle FloatingOnly a where
  describeDeco _ = "floating only"
  shrink _ _ r = r

  pureDecoration _ wh ht _ s@(Stack fw _ _) _ (w,Rectangle x y wid _) =
        if w == fw || not (isInStack s w)
          then Just $ Rectangle (fi nx) y nwh (fi ht)
          else Nothing
      where nwh = min wid $ fi wh
            nx  = fi x + wid - nwh

floatingOnly :: (Eq a, Shrinker s) => s -> Theme
         -> l a -> ModifiedLayout (Decoration FloatingOnly s) l a
floatingOnly s c = decoration s c FloatingOnly

