ManageHooks
===========

Заголовки и импорты
-------------------

> {-# LANGUAGE NoMonomorphismRestriction #-}
> module MyManageHooks 
>   (myManageHook, myFocusHook, pidginConfig)
>   where

> import Data.Char
> import Data.Maybe (isJust)
> import Foreign.C.Types (CLong)

> import XMonad
> import qualified XMonad.StackSet as W
> import XMonad.Hooks.ManageHelpers hiding (C)
> import XMonad.AppGroups
> import XMonad.Pidgin
> import XMonad.Utils
> import XMonad.Store

> import CommonFunctions
> import GroupsSetup     (appsConfig)

Правила для окон
----------------

Главное определение.

> myManageHook =
>     manageMenus <+>
>     ignoreDesktop <+>
>     gnuplot <+>
>     psi <+>
>     basehooks <+>
>     manageDialogs <+>
>     specialScreens <+>
>     floatPlasma

> psi = className =? "groupchat" --> doF (W.shift "im")

Сделать плавающими отрывающиеся меню

> manageMenus = composeAll [ checkMenu --> doFloat, checkKdeOverride --> doFloat, checkOnTop --> doFloat ]
>   where
>     checkMenu = isInProperty "_NET_WM_WINDOW_TYPE" "_NET_WM_WINDOW_TYPE_MENU"
>     checkKdeOverride = isInProperty "_NET_WM_WINDOW_TYPE" "_KDE_NET_WM_WINDOW_TYPE_OVERRIDE"
>     checkOnTop = isInProperty "_NET_WM_STATE" "_NET_WM_STATE_ABOVE" <||> isInProperty "_NET_WM_STATE" "_NET_WM_STATE_STAYS_ON_TOP"

Сделать плавающими диалоги

> manageDialogs = isDialog --> doFloat

Helper to read a property

> getProp :: Window -> Atom -> X (Maybe [CLong])
> getProp w a = withDisplay $ \dpy -> io $ getWindowProperty32 dpy a w

 Check if window has named atom with given value

> checkAtom name value = ask >>= \w -> liftX $ do
>                 a <- getAtom name
>                 val <- getAtom value
>                 mbr <- getProp w a
>                 case mbr of
>                   Just [r] -> return $ elem (fromIntegral r) [val]
>                   _ -> return False

> checkDesktop = checkAtom "_NET_WM_WINDOW_TYPE" "_NET_WM_WINDOW_TYPE_DESKTOP"

> ignoreDesktop :: ManageHook
> ignoreDesktop = composeAll [checkDesktop --> doIgnore]

Основная часть +ManageHook+-ов

> basehooks = composeOne (apps2hooks appsConfig) <+> moveToOwnWorkspace appsConfig <+> transience'

> gnuplot = title ~? "^Gnuplot" --> createAndMove False (Just 1) "plots"

> pidginConfig :: PidginWorkspaces
> pidginConfig = PidginWorkspaces {
>   moveBuddies = [(Groups ["FreeAlternative", "LUG"], Fixed "lug"),
>                  (AnyGroup, Corresponding)],
>   defaultWorkspace = "im" }

Сделать плавающими некоторые окна.

> floatPlasma = composeAll
>     [ className =? "Qt-subapplication" --> doFloat,
>       title =? "Qt-subapplication" --> doFloat,
>       title =? "Recently opened documents" --> doCenterFloat,
>       className ~? "[pP]lasma-desktop" --> doFloat,
>       className =? "XCalendar" --> doFloat,
>       className =? "Qwerty.py" --> doCenterFloat,
>       className =? "Palette_editor.py" --> doCenterFloat,
>       className =? "hcheckersc.py" --> doCenterFloat,
>       title =? "Копирование" --> doFloat,
>       title =? "Перемещение" --> doFloat ]

> specialScreens = composeAll $
>     [ role =? "gimp-toolbox" --> moveToDashboard ] ++
>     [ title =? t --> moveToDashboard | t <- panels ] ++ 
>     [ className =? "scribus-ng" --> createAndMove True (Just 0) "scribus" ] 
>   where
>     panels = ["История действий", "Палитра страниц", "Свойства", "Слои"]
>     moveToDashboard = doF (W.shift "dashboard2")

> myFocusHook :: Query Bool
> myFocusHook = allOf $ [
>   not `fmap` (className =? "Pidgin"),
>   not `fmap` (className =? "Konqueror"),
>   not `fmap` (className =? "Iceweasel") ]

