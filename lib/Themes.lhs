Themes — настройки внешнего вида
================================

Заголовки и импорты
-------------------

> module Themes where

Импорты из стандартной библиотеки.

> import qualified Data.Map as M
> import Data.Ratio ((%))
> import Data.Char (isAlphaNum)

Импорты из XMonad и xmonad-contrib

> import XMonad
> import XMonad.Layout.Decoration
> import XMonad.Layout.DecorationAddons
> import XMonad.Layout.ShowWName
> import XMonad.Prompt
> import XMonad.Layout.Groups.Examples
> import XMonad.Actions.GridSelect
> import XMonad.Actions.ShowText

> import qualified Debug.Trace as Trace

Определения
-----------

Параметры соединения с сервером MPD: имя хоста и порт.

> mpdHost = "192.168.1.103"
> mpdPort = 6600

Шрифт.

> myFont = "xft:Ubuntu-11"

Тема для +X.Prompt+.

> myXPConfig = def {
>   font    = myFont,
>   bgColor = "black",
>   fgColor = "white" }

Тема для +X.A.GridSelect+.

> gsTemplate :: HasColorizer a => GSConfig a
> gsTemplate = def {
>   gs_cellwidth = 300,
>   gs_cellheight = 40,
>   gs_cellpadding = 5
> }

> myGSConfig :: HasColorizer a => GSConfig a
> myGSConfig = gsTemplate {
>   gs_navigate = myNavigation,
>   gs_font     = myFont }

Тема для +X.A.GridSelect+, с поиском

> searchGS :: HasColorizer a => GSConfig a
> searchGS = gsTemplate {
>   gs_navigate = search,
>   gs_font     = myFont }

-- | Embeds a key handler into the X event handler that dispatches key
-- events to the key handler, while non-key event go to the standard
-- handler.
makeXEventhandler :: ((KeySym, String, KeyMask) -> TwoD a (Maybe a)) -> TwoD a (Maybe a)
makeXEventhandler keyhandler = fix $ \me -> join $ liftX $ withDisplay $ \d -> liftIO $ allocaXEvent $ \e -> do
                             maskEvent d (exposureMask .|. keyPressMask .|. buttonReleaseMask) e
                             ev <- getEvent e
                             if ev_event_type ev == keyPress
                               then do
                                  (ks,s) <- lookupString $ asKeyEvent e
                                  return $ do
                                      mask <- liftX $ cleanMask (ev_state ev)
                                      keyhandler (fromMaybe xK_VoidSymbol ks, s, mask)
                               else
                                  return $ stdHandle ev me

Определения горячих клавиш для +searchGS+:

> search :: TwoD a (Maybe a)
> search = makeXEventhandler $ shadowWithKeymap keymap handler
>   where keymap = M.fromList $ makeKeymap [
>            (xK_Escape, cancel)
>           ,(xK_Return, select)
>           ,(xK_Left  , move (-1,0) >> search)
>           ,(xK_Right , move (1,0) >> search)
>           ,(xK_Down  , move (0,1) >> search)
>           ,(xK_Up    , move (0,-1) >> search)
>           ,(xK_Tab   , moveNext >> search)
>           ,(xK_BackSpace, transformSearchString (\s -> if (s == "") then "" else init s) >> search)
>           ]
>
>         handler (_,s,_) | not (null s) && all isAlphaNum s = do
>           transformSearchString (++ s)
>           search
>         handler (sym,s,mask) = do
>           cancel

Определения горячих клавиш для +myGSConfig+:

> myNavigation :: TwoD a (Maybe a)
> myNavigation = makeXEventhandler $ shadowWithKeymap keymap handler
>   where keymap = M.fromList $ makeKeymap [
>            (xK_Escape, cancel)
>           ,(xK_Return, select)
>           ,(xK_slash , substringSearch myNavigation)
>           ,(xK_Left  , move (-1,0)  >> myNavigation)
>           ,(xK_h     , move (-1,0)  >> myNavigation)
>           ,(xK_Right , move (1,0)   >> myNavigation)
>           ,(xK_l     , move (1,0)   >> myNavigation)
>           ,(xK_Down  , move (0,1)   >> myNavigation)
>           ,(xK_j     , move (0,1)   >> myNavigation)
>           ,(xK_k     , move (0,-1)  >> myNavigation)
>           ,(xK_Up    , move (0,-1)  >> myNavigation)
>           ,(xK_y     , move (-1,-1) >> myNavigation)
>           ,(xK_i     , move (1,-1)  >> myNavigation)
>           ,(xK_n     , move (-1,1)  >> myNavigation)
>           ,(xK_m     , move (1,-1)  >> myNavigation)
>           ,(xK_space , setPos (0,0) >> myNavigation)
>           ]
>         -- The navigation handler ignores unknown key symbols
>         handler (_,s,_) | not (null s) && all isAlphaNum s = do
>           myNavigation
>         handler _ = cancel


> makeKeymap = concatMap allMasks
>   where
>     allMasks (key,action) = [((0,   key), action),
>                              ((8192,key), action)]

Обычная тема для вкладок:

> deco = def {activeColor       = activeDecoColor,
>                      activeTextColor   = "#000000",
>                      inactiveColor     = inactiveDecoColor,
>                      urgentColor       = "#EABA5C",
>                      inactiveTextColor = "#000000",
>                      activeBorderColor = activeDecoColor,
>                      inactiveBorderColor = inactiveDecoColor,
>                      decoHeight = 24,
>                      fontName = myFont}

Тема для вкладок с кнопками:

> decoB = defaultThemeWithButtons {activeColor = myFocusedBorderColor,
>                      activeTextColor = "#ffffff",
>                      fontName = myFont}

> inetTTC = def {
>    vRatio = 2%3,
>    tabsTheme = deco }

Список рабочих мест при запуске

> myWorkspaces    = ["dashboard", "dashboard2"]

> myFocusedBorderColor = "#E9B61C"

> inactiveDecoColor = "#D4D7D0"
> activeDecoColor = myFocusedBorderColor -- "#C2BFA5"

> myBorderWidth :: Dimension
> myBorderWidth = 2

Используемый терминал

> myTerminal :: String
> myTerminal = "konsole"

> textTerminal :: String
> textTerminal = "gnome-terminal --disable-factory --class=Text-terminal"

> mySWNConfig :: SWNConfig
> mySWNConfig = def {
>  -- swn_font = "-*-lucida-medium-r-*-*-*-140-*-*-*-*-iso10646-1"
>   swn_font = "xft:Ubuntu-24"
> }

> mySTC :: ShowTextConfig
> mySTC = def { st_font = "xft:Ubuntu-24" }

