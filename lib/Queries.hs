
module Queries where

import Control.Monad
import Data.Char
import Text.Regex.Posix

import XMonad
import qualified XMonad.StackSet as W

import XMonad.Actions.DynamicWorkspaces
import XMonad.Util.WindowProperties
import XMonad.Util.WindowPropertiesRE hiding ((~?))
import XMonad.Utils
import XMonad.Store
import XMonad.Prompt.Input

import CommonFunctions
import Themes

byTitle :: X ()
byTitle = do
  x <- inputPrompt myXPConfig "Title regexp"
  whenJust x $ \regex -> do
      let qry = title ~? regex
      wins <- matchingWindows qry
      when (not $ null wins) $ do
        let wksp = filter isAlpha regex
        addWorkspace wksp
        forM_ wins $ \w -> windows (W.shiftWin wksp w)

