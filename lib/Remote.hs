
module Remote where

import Control.Monad (when)
import Data.Monoid
-- import Graphics.X11.XInput as XI
import XMonad
import XMonad.Utils (setRootAtom, getRootAtom)

import Notify
import MPD

-- xinputHandler :: XI.EventCookie -> X All
-- xinputHandler e = do
--   return (All True)

ifMPD :: X () -> X () -> X ()
ifMPD x y = do
  mode <- getMPDMode
  if mode then x else y

whenMPD :: X () -> X ()
whenMPD action = do
  mode <- getMPDMode
  when mode action

getMPDMode :: X Bool
getMPDMode = do
  xs <- getRootAtom "MPD_MODE"
  return $ case xs of
            ["TRUE"] -> True
            _        -> False

toggleMPDMode :: X ()
toggleMPDMode = do
  mode <- getMPDMode
  case mode of
    True ->  do
             setRootAtom "MPD_MODE" "FALSE"
             notifySend "MPD" "Toggle mode" "MPD mode is now OFF."
    False -> do
             setRootAtom "MPD_MODE" "TRUE"
             notifySend "MPD" "Toggle mode" "MPD mode is now ON."

remotePlay :: X ()
remotePlay = whenMPD $ do
  mpdPlay
  song <- mpdSong
  notifySend "MPD" "Playing" song

remotePause :: X ()
remotePause = whenMPD $ do
  pausePlaying
  song <- mpdSong
  notifySend "MPD" "Paused" song

remoteStop :: X ()
remoteStop = whenMPD $ do
  stopPlaying
  notifySend "MPD" "Stopped" "MPD stopped"

remotePrevious :: X ()
remotePrevious = whenMPD $ do
  previousTrack
  song <- mpdSong
  notifySend "MPD" "Now playing" song

remoteNext :: X ()
remoteNext = whenMPD $ do
  nextTrack
  song <- mpdSong
  notifySend "MPD" "Now playing" song

