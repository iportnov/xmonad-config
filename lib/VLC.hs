{-# LANGUAGE TemplateHaskell #-}
module VLC (toggleVlc) where

import XMonad

import DBus
import DBus.Client
import DBus.TH.EDSL

interface "org.mpris.MediaPlayer2.vlc"
    "/org/mpris/MediaPlayer2"
    "org.mpris.MediaPlayer2.Player" Nothing
    [
      "Previous" =:: Return ''(),
      "Next" =:: Return ''(),
      "Stop" =:: Return ''(),
      "Play" =:: Return ''(),
      "Pause" =:: Return ''(),
      "PlayPause" =:: Return ''()
    ]

toggleVlc :: X ()
toggleVlc = io $ do
  dbus <- connectSession
  playPause dbus
