Общие функции
=============

Заголовок и импорты
-------------------

> module CommonFunctions
>   (
>    xrandr,
>    unmapEventHook,
>    modkey
>   ) where

Импорты из стандартной библиотеки:

> import Control.Monad (filterM, when)
> import qualified Control.Exception as E
> import System.FilePath.Glob
> import System.FilePath
> import System.Environment (getEnv)
> import Data.Maybe
> import Data.Monoid
> import Data.List
> import qualified Data.Map as M

Импорты из дополнительных библиотек (X11 и какой-нибудь библиотеки для регулярных выражений,
например regex-posix).

> import Graphics.X11.Xlib.Extras
> import Text.Regex.Posix ((=~))

Импорты из XMonad

> import XMonad
> import qualified XMonad.StackSet as W

Из xmonad-contrib

> import XMonad.Util.NamedWindows
> import XMonad.Layout.LayoutCombinators
> import XMonad.Layout.Minimize

> import XMonad.Actions.DynamicWorkspaces
> import XMonad.Actions.GridSelect
> import XMonad.Actions.OnScreen
> import XMonad.Prompt.Input
> import XMonad.Utils

Модуль из конфига.

> import Themes

Определения
-----------

> modkey :: String
> modkey = "M1-x"

> xrandr orient = "xrandr -o " ++ orient ++ "; xrandr --output 'VGA-1' --right-of 'DVI-I-1' --mode '1280x1024'"

При закрытии (точнее, unmap) окна — удалить текущее рабочее пространство, если оно осталось пустым.
Пространство "dashboard" — не удалять.

> unmapEventHook :: Event -> X All
> unmapEventHook (UnmapEvent {}) = do
>   current <- getCurrentWorkspace
>   when (not $ "dashboard" `isPrefixOf` current || "my-" `isPrefixOf` current) $
>       removeEmptyWorkspace
>   return (All True)
> unmapEventHook _ = return (All True)

