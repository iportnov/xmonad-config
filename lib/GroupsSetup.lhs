Группы приложений
=================

Заголовок и импорты
-------------------

> {-# LANGUAGE NoMonomorphismRestriction #-}
> module GroupsSetup (appsConfig, defaultFM) where

> import XMonad.Util.WindowProperties
> import XMonad.Util.WindowPropertiesRE
> import XMonad.Actions.GridSelect
> import XMonad.Utils
> import XMonad.AppGroups

Импорты других модулей из конфига:

> import Themes
> import Layouts (imWorkspaces)
> import Commands
> import CommonFunctions (modkey)

Файл-менеджер по умолчанию:

> defaultFM = "dolphin"

> regex = C . RE . ClassName

Список групп
------------

Собственно список групп приложений:

> myApps = 
>   [ [C "Epiphany-browser", C "Kontact",
>      C "Iceweasel", C "Firefox", C "Chromium", C "Vivaldi-stable",
>      C "Opera", C "Arora" ]     `orSpawn` "firefox"          ~> "inet"     `on` (modkey ++ " w") `named` "web",
>     [C "Icedove", C "Kontact" ] `orSpawn` "icedove"          ~> "news"     `on` (modkey ++ " y") `named` "mail",
>     [C "Liferea", C "RSSOwl"]   `orSpawn` "rssowl"          ~>> "news"                   `named` "rss",
>     group [C "Inkscape", C "Eog", C "Gwenview", C "Dia",
>                    C "MyPaint"]                              ~> "graphics" `on` (modkey ++ " d"),
>     group         [regex "Gimp"]                             ~> "gimp"     `on` (modkey ++ " g"),
>     group         [C "F-spot", regex "Digikam",
>                    regex "Darktable"]                        ~> "photo"    `on` (modkey ++ " p"),
>     [C "Gnome-terminal",
>      C "konsole"]               `orSpawn` myTerminal        ~>> "term"     `on` (modkey ++ " t"),
>     [C "Gedit", C "Leafpad",
>      C "Text-terminal", C "Gvim", C "nvim-qt",
>      C "kate", C "kwrite", C "Emacs"] `orRun` textEditors   ~>> "text"     `on` (modkey ++ " e"),
>     [regex "libreoffice",
>      C "TeXmacs"]               `orSpawn` recent ["doc"]     ~> "office"   `on` (modkey ++ " o"),
>     [C "Evince", C "Okular"] `orSpawn` recent ["pdf","djvu"] ~> "docs"     `on` (modkey ++ " k"),
>     [C "Nautilus", C "dolphin", C "Konqueror", C "Pcmanfm",
>                    C "Krusader"] `orSpawn` defaultFM        ~>> "files"    `on` (modkey ++ " f"),
>     group [C "Amarok", C "Rhythmbox", C "Ario",
>            C "Sonata", regex "Audacious" ]                   ~> "music"    `on` (modkey ++ " a"),
>     group         [C "MPlayer", C "Totem"]                  ~>> "video"    `on` (modkey ++ " v"),
>     group         [C "Wxmaxima"]                             ~> "math"     `on` (modkey ++ " m"),
>     [C "Pidgin", C "Kopete"] `orSpawn` "pidgin"              ~> "im"       `on` (modkey ++ " i"),
>     [C "Virt-manager"]           `orSpawn` "virt-manager"    ~> "virtual"  `on` (modkey ++ " u")]


Для таких окон специальные рабочие места создаваться не будут.

> notCreateWorkspace = ["krunner", "gmrun", "plasma", "plasmashell", "gcolor2", "ksnapshot", "palette",
>                       "xmessage", "unknown", "xfce4", "pidgin", "hcheckersc"]

> appsConfig :: AppsConfig
> appsConfig = AppsConfig myApps searchGS myGSConfig searchGS notCreateWorkspace $ fromGroups $
>   [["inet", "news", "mail", "text", "office", "video", "photo", "scribus", "graphics", "gimp", "blender"],
>    ["torrents", "plots", "dashboard2"] ++ imWorkspaces]

