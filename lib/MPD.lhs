Управление MPD
==============

Заголовок и импорты
-------------------

> module MPD 
>   (togglePlaying,
>    mpdPlay,
>    pausePlaying,
>    stopPlaying,
>    nextTrack,
>    previousTrack,
>    mpdSong,
>    showMpd
>   ) where

> import qualified Data.Map as M
> import XMonad

Импорт из пакета libmpd.

> import Network.MPD

Модуль конфига.

> import Notify
> import Themes (mpdHost, mpdPort)

Определения
-----------

Вспомогательная «обёртка».

> mpd :: MPD a -> X ()
> mpd action = do
>   io $ withMPDEx mpdHost mpdPort "" action
>   return ()

> mpd' :: MPD a -> X a
> mpd' action = do
>   io $ do
>     r <- withMPDEx mpdHost mpdPort "" action
>     case r of
>       Right result -> return result
>       Left err -> fail (show err)

Играть/пауза.

> togglePlaying :: X ()
> togglePlaying = mpd toggle
>   where
>     toggle = status >>= \st ->
>       case stState st of
>         Playing -> pause True
>         _       -> play Nothing

> mpdPlay :: X ()
> mpdPlay = mpd (play Nothing)

> pausePlaying :: X ()
> pausePlaying = mpd (pause True)

> stopPlaying :: X ()
> stopPlaying = mpd stop

> nextTrack :: X ()
> nextTrack = mpd next

> previousTrack :: X ()
> previousTrack = mpd previous

> mpdSong :: X String
> mpdSong = showSong `fmap` mpd' currentSong
>   where
>     showSong (Just s) = get Artist s ++ ": " ++ get Title s
>     showSong Nothing  = "Not playing"
>     
>     get m s = case M.lookup m (sgTags s) of
>                 Nothing -> "Unknown"
>                 Just list -> unwords (map toString list)

> showMpd :: X ()
> showMpd = notifySend "MPD" "Now playing" =<< mpdSong

