{-# LANGUAGE OverloadedStrings #-}

module Info where

import Control.Monad
import Control.Concurrent
import DBus.Client

import XMonad
import XMonad.Utils

data Info = Info {
    iCurrentWorkspace :: String,
    iCurrentLayout :: String
    -- to be continued
  }

-- LogHook
exportInfo :: Chan Info -> X ()
exportInfo chan = do
  wksp <- getCurrentWorkspace
  layout <- getLayout
  let info = Info wksp layout
  io $ writeChan chan info
  
-- Startup Hook
dbusExporter :: Chan Info -> X ()
dbusExporter chan = void $ io $ forkIO $ do
    dbus <- connectSession
    export dbus "/org/xmonad/info"
      defaultInterface {
        interfaceName = "org.xmonad.info",
        interfaceMethods = [
            autoMethod "getCurrentLayout" mLayout,
            autoMethod "getCurrentWorkspace" mWorkspace
          ]
        }
  where
    mLayout :: IO String
    mLayout = do
      info <- readChan chan
      return $ iCurrentLayout info

    mWorkspace :: IO String
    mWorkspace = do
      info <- readChan chan
      return $ iCurrentWorkspace info

