Layouts
=======

Заголовок и импорты
-------------------

> {-# LANGUAGE ExistentialQuantification, FlexibleInstances, FlexibleContexts, MultiParamTypeClasses, TypeSynonymInstances, DeriveDataTypeable, NoMonomorphismRestriction #-}
> module Layouts where

> import Data.Ratio ((%))

> import XMonad hiding ((|||))
> import XMonad.Hooks.ManageDocks hiding (L,R)

> import WindowPropertiesRE

Импорты из xmonad-contrib.

> import XMonad.Layout.ShowWName
> import XMonad.Layout.AutoMaster
> import XMonad.Layout.Column
> import XMonad.Layout.Decoration
> import XMonad.Layout.PerWorkspace
> import XMonad.Layout.LayoutCombinators
> import XMonad.Layout.Named
> import XMonad.Layout.GridVariants
> import XMonad.Layout.IM
> import XMonad.Layout.MagicFocus
> import XMonad.Layout.CenteredMaster
> import XMonad.Layout.OneBig
> import XMonad.Layout.NoBorders
> import XMonad.Layout.TwoPane
> import XMonad.Layout.ButtonDecoration
> import XMonad.Layout.WindowSwitcherDecoration
> import XMonad.Layout.SimpleDecoration
> import XMonad.Layout.NoFrillsDecoration
> import XMonad.Layout.DraggingVisualizer
> import XMonad.Layout.Tabbed
> import XMonad.Layout.Minimize
> import XMonad.Layout.Simplest
> import qualified XMonad.Layout.Groups as G
> import XMonad.Layout.LayoutBuilderP
> import XMonad.Layout.BinarySpacePartition
> import XMonad.Layout.BorderResize

-- import XMonad.Layout.AutoComboP

> import XMonad.Layout.ThreeColumns
> import XMonad.Layout.Master
> import XMonad.Layout.DwmStyle
> import XMonad.Layout.Spacing
> import XMonad.Layout.Magnifier
> import XMonad.Layout.IfMax

> import qualified XMonad.Layout.WindowNavigation as Nav

Модуль конфига.

-- import XMonad.Layout.InRegion

> import Themes

Вспомогательные определения
---------------------------

Группы чатов.

> lugChats = RE (Title "lug-mgn" `Or` Title "math")
> xmonadChats = RE (Title "xmonad")
> programmingChats = RE (Title "programming" `Or` Title "haskell" `Or` Title "python" `Or` Title "java")

> fullscreenRect = Just $ relBox 0 0 1 1

Модификатор для всяческих ростеров.

> withRoster = withIM (1%10) (Role "buddy_list")

> isGfxPanel = Role "gimp-toolbox" `Or` Role "Brush selector" `Or` Role "toolbox_window" `Or` Role "Layers"

Определения Layout-ов
---------------------

Простой layout со вкладками.

> tabs = tabbed shrinkText deco

> decorated = dwmStyle shrinkText deco

Layout для окон Pidgin

> imlayout = noFrillsDeco shrinkText deco $ magnifiercz 1.2 $ Grid 1

> grid = named "grid" $ Grid (2)
> tiled = Tall 1 (1/100) (1/2)
> dwmtile = named "dwm" $ decorated $ tiled
> forterm = named "term" $ minimize $ dwmStyle shrinkText deco $ magnifiercz' 1.2 $ tiled

> full = decorated $ Full

> inet = mastered (1/100) (1/2) $ autoMaster 1 (1/100) tabfull

 inet = IfMax 1 (inRegion 0.15 0 0.7 1 $ tiled) $ mastered (1/100) (1/2) $ autoMaster 1 (1/100) tabfull

> mix = named "mix" $ Tall 1 (1/100) (2/3)
> mirrored = named "mirror" $ decorated $ Mirror tiled
> mirrored' = named "term'" $ decorated $ Mirror $ magnifiercz' 1.2 $ tiled
> -- forgimp = named "gimp" $ reflectHoriz (Tall 1 (1/100) (1/4))
> zgrid =  magicFocus (centerMaster grid)
> autogrid = named "autogrid" (autoMaster 1 (1/100) grid)
> autotall = Mirror (autoMaster 1 (1/100) grid)
> autogrid2 = autoMaster 2 (1/100) grid
> books = named "books" (Tall 1 (1/100) (2/3))

> tabgrid = named "tabgrid" $ addTabs shrinkText deco $ G.group Simplest autotall
> tabfull  = addTabs shrinkText deco Simplest
> tabtall = named "tabtall" $ addTabs shrinkText deco $ G.group Simplest $ Tall 1 (1/100) (1%2)
> masteredTabs p = mastered (1/100) p $ addTabs shrinkText deco $ G.group Simplest grid

> bsp = named "BSP" $ borderResize emptyBSP

> forim = named "im" $ minimize $ withRoster $ {- spacing 4 -} (imlayout ||| tabs ||| bsp)
> column = Column 1.8
> onebig = named "onebig" $ (OneBig (3/4) (3/4))
> threecol = decorated $ ThreeCol 1 0.03 (1%3)

 fortext = autoCombineTwoP 0.1 0.1 0.03 (Mirror $ TwoPane 0.03 0.5) mix grid (ClassName "Gvim")

> fortext = dwmtile

> withButtons = buttonDeco shrinkText decoB

> draggable layout = windowSwitcherDecorationWithButtons shrinkText decoB (draggingVisualizer $ layout)

> imWorkspaces = ["im", "Graphics", "Linux",
>                 "Programming", "Jabber", "lug"]

Главное определение
-------------------

> myLayout = {-showWName' mySWNConfig $ -} smartBorders $
>            Nav.configurableNavigation (Nav.navigateBrightness 0.0) $
>            avoidStruts $
>            onWorkspace "inet" (tabfull ||| inet ||| mirrored ||| masteredTabs 0.5 ||| full) $
>            onWorkspace "news" full $
>            onWorkspace "text" (fortext ||| threecol ||| autogrid2 ||| dwmtile ||| mirrored' ||| books ) $
>            onWorkspace "files" (full ||| dwmtile ||| autogrid) $
>            onWorkspaces imWorkspaces forim $
>            onWorkspace "term" (forterm ||| mirrored' ||| full ||| bsp) $
>            onWorkspace "gimp" (tabtall ||| dwmtile ||| column) $
>            onWorkspace "photo" (full ||| mirrored) $
>            onWorkspace "scribus" (tabgrid ||| full) $
>            onWorkspace "plots" (grid ||| autotall ||| full) $
>            onWorkspace "virtual" (masteredTabs 0.45 ||| tabtall ||| full) $
>            onWorkspace "dashboard2" (threecol ||| tabtall ||| full ||| bsp) $
>            (dwmtile ||| mirrored ||| tabfull ||| threecol ||| full ||| bsp)

