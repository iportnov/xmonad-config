Определения горячих клавиш
==========================

Заголовок и импорты
-------------------

> module KeyBindings 
>   (myKeys,
>    addKeys,
>    myMouseBindings)
>   where

Импорты из стандартной библиотеки:

> import System.Exit
> import qualified Data.Map as M
> import Control.Concurrent

Импорты из XMonad:

> import XMonad
> import qualified XMonad.StackSet as W

Импорты из xmonad-contrib:

> import XMonad.Util.Paste
> import XMonad.Hooks.ManageDocks -- hiding (L,R)

> import XMonad.Actions.DwmPromote
> import XMonad.Actions.GridSelect
> import XMonad.Actions.GroupNavigation
> import XMonad.Actions.Warp
> import XMonad.Actions.DynamicWorkspaces
> import qualified XMonad.Actions.FlexibleManipulate as Flex

> import XMonad.Layout.SubLayouts
> import XMonad.Layout.BinarySpacePartition
> import qualified XMonad.Layout.Groups.Examples as Ex
> import XMonad.Layout.Magnifier
> import XMonad.Layout.Minimize
> import XMonad.Utils
> import XMonad.AppGroups
> import XMonad.Pidgin
> import XMonad.Store

Импорты других модулей конфига:

> import MPD
> import VLC
> import Volume
> import CommonFunctions
> import Themes
> import GroupsSetup
> import Mouse
> import Remote
> import Queries
> import Xosd
> import Notify
> import Commands
> import WacomConfig (wconfig)

> import qualified XMonad.Wacom as Wacom
> import XMonad.Wacom.Daemon (setTabletMapArea, toggleRingMode, reloadConfig)

Основные сочетания клавиш
-------------------------

> myKeys conf = M.fromList $

Запустить терминал:

>     [ ((shiftMask, xK_Menu),   spawn myTerminal),
>       ((0, 0x1008FF41),        switchToApp appsConfig "office"),
>       ((0, 0x1008FF42),        switchToApp appsConfig "text"),
>       ((0, 0x1008FF43),        switchToApp appsConfig "term"),
>       ((0, 0x1008FF44),        switchToApp appsConfig "rss"),
>       ((0, 0x1008FF45),        switchToApp appsConfig "im"),

Увеличить/уменьшить количество мастер-окон:

>     ((mod4Mask              , xK_comma ), sendMessage (IncMasterN 1)),
>     ((mod4Mask              , xK_period), sendMessage (IncMasterN (-1))),
>     ((mod4Mask .|. shiftMask, xK_space),  setLayout $ XMonad.layoutHook conf)
>     ] -- ++
 
mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
mod-shift-{w,e,r}, Move client to screen 1, 2, or 3

     [((mod4Mask .|. mask, key), f sc)
      | (key, sc) <- zip [xK_F2, xK_F1] [0..]
      , (f, mask) <- [(viewScreen, 0), (sendToScreen, shiftMask)]]

Расширенные сочетания клавиш
----------------------------

Запуск всякого разного:

> addKeys = [ -- ("M1-<F2>",         gnomeRun),
>           ("M1-<F2>",         spawn "gmrun"),
>           ("<Pause>",         spawn "qwerty.py -a -f -g 640x400"),
>           ("M-v",             vimsessions),

Закрыть окно

>           ("M1-<F4>",         kill),

Переключиться на следующий layout

>           ("M-<Space>",       sendMessage NextLayout),
>           --
>           (modkey ++ " x",          sendKey mod1Mask xK_x),

>           ("M1-e",             windows (W.greedyView "dashboard")),

Выбрать определённый layout

>           ("M-f",             chooseLayout "Full"),
>           ("M-o",             chooseLayout "onebig"),
>           ("M-u",             chooseLayout "autogrid"),



>           ("M-<Up>",          spawn "/home/portnov/bin/rotate-monitor HDMI-0 normal"),
>           ("M-<Right>",       spawn "/home/portnov/bin/rotate-monitor HDMI-0 left"),

Спрятать/показать панели, if any.

>           ("M-b",             sendMessage $ ToggleStruts),

Выбрать окно и притащить его на текущее рабочее место:

>           ("M1-w",            bringSelected searchGS),
>           ("M1-q",            selectQuery searchGS ),

Выбрать окно среди всех и перейти к нему:

>           ("M1-/",            goToSelected searchGS),

Выбрать окно на текущем рабочем месте:

>           ("M1-<Tab>",        windows W.focusDown),

Выбрать рабочее место:

>           ("M-<Tab>", selectWorkspaceOn appsConfig Nothing  ),
>           ("M-<F2>",  selectWorkspaceOn appsConfig (Just 1) ),
>           ("M-<F1>",  selectWorkspaceOn appsConfig (Just 0) ),

>           ("M-c",  toNewWorkspace myXPConfig ),
>           ("M-s",  do
>                      wksp <- getCurrentWorkspace
>                      storeWorkspace wksp
>                      notifySend "Store" "Workspace stored" ("Workspace " ++ wksp ++ " is stored") ),
>           ("M-S-s", readStoredProps),
>           ("M-t",  byTitle ),

Переместить окно на выбранное рабочее место:

>           ("M-m",             gridselectWorkspace searchGS W.shift),

Перейти к предыдущему окну:

>           ("M-<Backspace>",   nextMatch History (return True)),

Пересчитать размеры окна

>           ("M-n",             refresh),

Переключиться на следующее/предыдущее окно:

>           ("M-j",             windows W.focusDown),
>           ("M-k",             windows W.focusUp  ),

Действия мыши:

>           ("M1-k",            sendButtonPress 4),  -- колесо вверх
>           ("M1-j",            sendButtonPress 5),  -- колесо вниз
>           ("<XF86ScrollUp>",  sendButtonPress 4),
>           ("<XF86ScrollDown>", sendButtonPress 5),
>           ("M-h",             sendButtonPress 1),  -- клик

Поменять текущее окно с мастер-окном. В случае layout «im» — развернуть/восстановить окно.

>           ("M-<Return>",      dwmpromote),

Поменять текущее окно со следующим/предыдущим.

>           ("M-S-j",           caseLayoutOf [("tabgrid", Ex.swapDown)] (windows W.swapDown)),
>           ("M-S-k",           caseLayoutOf [("tabgrid", Ex.swapUp)]   (windows W.swapUp)),

Увеличить/уменьшить область мастер-окна.

>           ("M-e",             zoomOut),
>           ("M-r",             zoomIn),

>           ("M-z",             sendMessage MagnifyMore),
>           ("M-S-z",           sendMessage MagnifyLess),

           ("M-x",             withFocused minimizeWindow),
           ("M-S-x",           sendMessage RestoreNextMinimizedWin),

>           ("M-M1-<Left>",    sendMessage $ ExpandTowards L),
>           ("M-M1-<Right>",   sendMessage $ ShrinkFrom L),
>           ("M-M1-<Up>",      sendMessage $ ExpandTowards U),
>           ("M-M1-<Down>",    sendMessage $ ShrinkFrom U),
>           ("M-M1-C-<Left>",  sendMessage $ ShrinkFrom R),
>           ("M-M1-C-<Right>", sendMessage $ ExpandTowards R),
>           ("M-M1-C-<Up>",    sendMessage $ ShrinkFrom D),
>           ("M-M1-C-<Down>",  sendMessage $ ExpandTowards D),

>           ("M-w",            sendMessage $ Swap),
>           ("M-M1-w",         sendMessage $ Rotate),

Вернуть плавающее окно в обычный (tiled) режим.

>           ("M-S-f",           withFocused $ windows . W.sink),

Редактировать конфиг xmonad.

>           ("M-<Home>",        vimsession "xmonad"),

Заблокировать экран.

>           ("M-l",             spawn "xscreensaver-command -lock"),

Multimedia keys. 

>           ("C-S-<XF86AudioPrev>",        previousTrack),
>           ("C-S-<XF86AudioNext>",        nextTrack),
>           ("<XF86AudioStop>",        stopPlaying),
>           ("<XF86AudioPlay>",        toggleAnyPlayer),
>           ("C-<XF86AudioPlay>",        toggleAnyPlayer),
>           ("<XF86AudioMute>",        toggleMute),

>           ("<XF86AudioLowerVolume>", changeVolumeBy (-500)),
>           ("<XF86AudioRaiseVolume>", changeVolumeBy   500),

           ("<XF86AudioLowerVolume>", io $ putStrLn "lower"),
           ("<XF86AudioRaiseVolume>", io $ putStrLn "raise"),

>           ("<XF86Search>",    switchToApp appsConfig "web" ),
>           ("<XF86HomePage>",  switchToApp appsConfig "files"),
>           ("<XF86Mail>",      switchToApp appsConfig "mail"),

>           (modkey++" j", do {spawn "pidgin" >> pidginConnect}),

           ("M1-t g", Wacom.withProfile (\profile -> notifySend "Wacom" "Wacom tablet" ("Current profile is " ++ profile))),

>           ("M-C-<F1>",        setTabletMapArea 0),
>           ("M-C-<F2>",        setTabletMapArea 1),
>           ("M1-C-S-r",        toggleRingMode (\name -> notifySend "Wacom" "Wacom tablet" ("Set ring mode: " ++ name))),
>           ("M-<F12>",         togglePidginRoster),

Сделать скриншот.

>           ("<Print>",         spawn "spectacle"),

Выход из xmonad.

>           ("M-S-q",           io $ exitWith ExitSuccess),

Перезапустить xmonad.

>           ("M-q",             do
>                               reloadConfig wconfig
>                               broadcastMessage ReleaseResources
>                               restart "xmonad" True)]

Привязки действий мыши.

> myMouseBindings (XConfig {XMonad.modMask = modMask}) = M.fromList $
>     -- mod-button1, Set the window to floating mode and move by dragging
>     [ ((modMask, button1), (\w -> focus w >> Flex.mouseWindow discrete w))
>     -- mod-button2, Raise the window to the top of the stack
>     , ((modMask, button2), (\w -> focus w >> dwmpromote))
>     -- mod-button3, Set the window to floating mode and resize by dragging
>     -- , ((modMask, button3), (\w -> focus w >> mouseResizeWindow w))
>     , ((modMask, button3), (\w -> focus w >> Flex.mouseWindow Flex.resize w))
>     ]
>   where
>     discrete x
>       | x < 0.1 = 0
>       | x > 0.9 = 1
>       | otherwise = 0.5

> isVlc :: Query Bool
> isVlc = className =? "vlc"

> toggleAnyPlayer :: X ()
> toggleAnyPlayer = do
>   withWindowSet $ \ss -> do
>     whenJust (W.peek ss) $ \w -> do
>       vlc <- runQuery isVlc w
>       io $ print vlc
>       if vlc
>         then toggleVlc
>         else togglePlaying >> showMpd

> zoomIn :: X ()
> zoomIn = caseLayoutOf [("im", sendMessage MagnifyMore),
>                        ("term", ifMaster (sendMessage Expand) (sendMessage MagnifyMore))]
>                       (sendMessage Expand)

> zoomOut :: X ()
> zoomOut = caseLayoutOf [("im", sendMessage MagnifyLess),
>                         ("term", ifMaster (sendMessage Shrink) (sendMessage MagnifyLess))]
>                        (sendMessage Shrink)

