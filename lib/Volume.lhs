Управление громкостью через ALSA
================================

Заголовок и импорты
-------------------

> module Volume
>   (changeVolumeBy,
>    toggleMute) where

> import Control.Concurrent (forkIO)
> import Control.Monad (when, forM, forM_)

> import XMonad

Импорт из пакета alsa-mixer.

> import Sound.ALSA.Mixer
> import Xosd

Определения
-----------

> getVolume volumeVal = do
>   volumes <- forM (channels $ value volumeVal) $ \ch -> do
>                  Just vol <- getChannel ch $ value volumeVal
>                  return vol
>   return $ maximum volumes

> setVolume volumeVal v = do
>   forM_ (channels $ value volumeVal) $ \ch -> do
>       putStrLn $ "Channel " ++ show ch ++ ": " ++ show v
>       setChannel ch (value volumeVal) v

> getPercent :: Integer -> Integer -> Integer -> Int
> getPercent min _ val | val < min = 0
> getPercent _ max val | val > max = 100
> getPercent min max val | otherwise =
>   fromIntegral $ (100 * (val - min)) `div` (max - min)

Изменить громкость (в канале Master) на i пунктов.

> changeVolumeBy :: Integer -> X ()
> changeVolumeBy i = do
>    percent <- do io $ withMixer "default" $ \mixer -> do
>                  Just control <- getControlByName mixer "Master"
>                  let Just playbackVolume = playback $ volume control
>                  (min, max) <- getRange playbackVolume
>                  vol <- getVolume playbackVolume
>                  if ((i > 0 && vol < max) || (i < 0 && vol > min))
>                    then do
>                      let newVolume = vol + i
>                      setVolume playbackVolume newVolume
>                      return $ getPercent min max newVolume
>                    else
>                      return $ getPercent min max vol
>    return ()

    ifXosdNotTooFast $ do
       io $ forkIO $ xosdGauge percent
       return ()

Включить/выключить звук (в канале Master).

> toggleMute :: X ()
> toggleMute = io $ withMixer "default" $ \mixer -> do
>    result <- getControlByName mixer "Master"
>    whenJust result $ \control ->
>      whenJust (playback $ switch control) $ \playbackSwitch -> do
>        mbsw <- getChannel FrontLeft playbackSwitch
>        whenJust mbsw $ \sw -> setChannel FrontLeft playbackSwitch $ not sw

