Mouse — действия мышью
======================

Заголовки и импорты
-------------------

> {-# LANGUAGE ForeignFunctionInterface #-}
> module Mouse
>   (sendButtonPress,
>    movePointerX)
>   where

> import Graphics.X11.XTest

> import XMonad
> import qualified XMonad.StackSet as W

Имитировать нажатие кнопки мыши.

> sendButtonPress :: Button -> X ()
> sendButtonPress button = do
>   withDisplay $ \dpy -> io (fakeButtonPress dpy button)

Переместить указатель мыши на вектор (dx,dy).

> movePointerX :: Int -> Int -> X ()
> movePointerX dx dy = do
>   sid <- withWindowSet (return . W.screen . W.current)
>   root <- asks theRoot
>   withDisplay $ \dpy -> io (movePointer dpy (fromIntegral sid) root dx dy)

