
module WacomConfig where

import qualified Data.Map as M
import XMonad

import System.Wacom.Types
import System.Wacom.Config
import XMonad.Wacom.Daemon
import XMonad.Wacom

import Notify

-- Tablet buttons:
--
-- 2
-- 3
-- 8
-- 9
--
-- (1)
--
-- 10
-- 11
-- 12
-- 13

toggleRing :: String
toggleRing = "ctrl alt shift r"

wconfig :: Config
wconfig =
  dfltConfig {
    -- tTouch = True,
    tMapAreas = ["1920x1200+0+0", "1920x1080+1920+0"],
    tProfiles = buildProfiles
                 [Profile "Default" [ringScrolls, ringPgUp] baseButtons,
                  Profile "Krita"   [ringBrackets, ringScrolls, ringOpacity, ringRotate] kritaButtons,
                  Profile "Blender" [ringScrolls, ringBrackets, ring46] blenderButtons
                 ]
  }

ringScrolls :: RingMode
ringScrolls = RingMode "Scroll" (Click 5) (Click 4)

ringBrackets :: RingMode
ringBrackets = RingMode "Brush size" (Key "[") (Key "]")

ringPgUp :: RingMode
ringPgUp = RingMode "PgUp/PgDown" (Key "PgDn") (Key "PgUp")

ringOpacity :: RingMode
ringOpacity = RingMode "Opacity" (Key "o") (Key "i")

ringRotate :: RingMode
ringRotate = RingMode "Rotate" (Key "ctrl ]") (Key "ctrl [")

ring46 :: RingMode
ring46 = RingMode "Orbit Left/Right" (Key "4") (Key "6")

baseButtons :: ButtonsMap
baseButtons = ButtonsMap $
  M.fromList [
    (1, Key toggleRing),
    (2, Key "ctrl"),
    (3, Key "shift"),
    (13, Key "ctrl z"),
    (12, Key "Delete")
  ]

blenderButtons :: ButtonsMap
blenderButtons = ButtonsMap $ M.fromList [
    (1, Key "shift"),
    (2, Key toggleRing),
    (8, Key "7"),
    (9, Key "ctrl"),
    (10, Key "3"),
    (11, Key "5")
  ] `M.union` unButtonsMap baseButtons

kritaButtons :: ButtonsMap
kritaButtons = ButtonsMap $ M.fromList [
    (1, Key "shift"),
    (2, Key toggleRing),
    (3, Key "ctrl"),
    (8, Key "alt shift m"),
    (9, Key "alt shift b"),
    (10, Key "alt shift n"),
    (11, Key "alt shift w")
  ] `M.union` unButtonsMap baseButtons

switchTabletProfile :: X ()
switchTabletProfile = 
    wacomProfiles Internal
      [(className =? "Krita" <||> className =? "krita", "Krita"),
       (className =? "Gimp", "Gimp"),
       (className =? "Inkscape", "Inkscape"),
       ((className =? "Blender" <||> className =? "blender"), "Blender"),
       (className =? "MyPaint", "MyPaint") ]
      (\profile -> do
                   io $ putStrLn $ "Set profile: " ++ profile
                   notifySend "Wacom" "Profile changed" ("Tablet profile changed to " ++ profile))

notifyOnPlug :: TabletDevice -> IO ()
notifyOnPlug dev = 
  case dStylus dev of
    Just stylus -> notifySendIO "Wacom" "Tablet Attached" $ "Tablet device attached: " ++ stylus
    Nothing -> return ()

notifyOnUnplug :: TabletDevice -> IO ()
notifyOnUnplug dev = 
  case dStylus dev of
    Just stylus -> notifySendIO "Wacom" "Tablet Removed" $ "Tablet device removed: " ++ stylus
    Nothing -> return ()

