{-# LANGUAGE TemplateHaskell #-}
module NotifyDbus where

import DBus.TH.EDSL
import DBus.TH.Introspection

-- Interface org.freedesktop.DBus.Peer
interface "org.freedesktop.Notifications" "/org/freedesktop/Notifications" "org.freedesktop.DBus.Peer" Nothing [
    "Ping" =:: Return ''(),
    "GetMachineId" =:: Return ''String
  ]

-- Interface org.freedesktop.Notifications
interface "org.freedesktop.Notifications" "/org/freedesktop/Notifications" "org.freedesktop.Notifications" Nothing [
    "GetCapabilities" =:: Return ''ListStr,
    "Notify" =:: ''String :-> ''Word32 :-> ''String :-> ''String :-> ''String :-> ''ListStr :-> ''DictStrVariant :-> ''Int32 :-> Return ''Word32,
    "CloseNotification" =:: ''Word32 :-> Return ''()
  ]

