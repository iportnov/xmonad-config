Управление libnotify
====================

Заголовок и импорты
-------------------

> module Notify
>   (notifySend, notifySendIO)
>   where

> import DBus
> import DBus.Client
> import qualified Data.Map as M

> import XMonad

> import DBus.Notify as Notify

> notifySend :: String -> String -> String -> X ()
> notifySend cat title text = io $ notifySendIO cat title text

> notifySendIO :: String -> String -> String -> IO ()
> notifySendIO cat title text = do
>   dbus <- connectSession
>   notify dbus $ blankNote {Notify.appName = cat, summary = title, body = Just $ Text text}
>   return ()

