{-# LANGUAGE DeriveDataTypeable, TypeSynonymInstances, MultiParamTypeClasses, FlexibleInstances, ScopedTypeVariables #-}

module Xosd where

import Control.Monad (when)
import Control.Concurrent
import Data.Time.Clock

import Graphics.XOSD as XOSD

import XMonad
import qualified XMonad.Util.ExtensibleState as XS

xosdTimeout :: Int
xosdTimeout = 1

xosdDisplay :: String -> X ()
xosdDisplay text = io $ do
 runXOSD [ Timeout 3
          , VAlign VAlignMiddle
          , HAlign HAlignCenter
          , Font "-*-lucida-bold-r-*-*-34-*-*-*-*-*-*-*"
          , XOSD.Color "LimeGreen"
          , XOSD.Display (String text)] 
      (const $ return ())

xosdGauge :: Int -> IO ()
xosdGauge percent = do
 runXOSD [ Timeout xosdTimeout
          , VAlign VAlignMiddle
          , HAlign HAlignCenter
          , Font "-*-lucida-bold-r-*-*-34-*-*-*-*-*-*-*"
          , XOSD.Color "LimeGreen"
          , XOSD.Display (Percent percent)] 
      (const $ return ())

type XosdState = Maybe UTCTime

instance ExtensionClass XosdState where
  initialValue = Nothing

saveXosdTime :: X ()
saveXosdTime = do
  time <- io $ getCurrentTime
  XS.put $ Just time

ifXosdNotTooFast :: X () -> X ()
ifXosdNotTooFast action = do
  now <- io $ getCurrentTime
  prev <- XS.get
  case prev of
    Nothing -> do
      saveXosdTime
      action
    Just time -> do
      when (diffUTCTime now time >= fromIntegral xosdTimeout) $ do
        saveXosdTime
        action

